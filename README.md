# **Mandiri Course slicing** (Test)

**development**
before to main, we should deploy to development for testing, if in development (ok)
we can merge to main for deployment

structure project

```
  src --
        |app
            -- component
                - footer
                - image-star
                - loading
                - navbar
                - products-items
            -- pages
                - home-page
                - products-detail
                - whistlist-page
            -- service
                - products.service.ts
            -- shared
                - products.model.ts
                - search.pipe.ts
                - shared-data.service.ts
        |assets
            - ( all images )
        |enviroment
            - enviroment.prod.ts
            - enviroment.ts

```


This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.2.10.
( **version 14.2.10** )

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

important, if the small component please generate use standalone=true,
`ng generate component component-name --standalone --skip-tests -inline-tempate --inline-style`

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

--demoapp main -> https://test-mandiri-bank.vercel.app/

for skips error npm install pleasae use 

```
npm install --legacy-peer-deps
```

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
