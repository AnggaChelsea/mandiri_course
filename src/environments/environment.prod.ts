export const environment = {
  production: true,
  localUrl: 'https://adorable-hem-cow.cyclic.app/project-course/',
  fireBase: 'https://auth-59b16.firebaseio.com/',
};
