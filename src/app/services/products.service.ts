import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { ProductCourse } from '../shared/products.model';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private http: HttpClient) { }

  getProduct() :Observable<ProductCourse>{
    return this.http.get<ProductCourse>(`${environment.localUrl}list-projects`) as Observable<ProductCourse>
  }
  dogetProductById(id: string | null): Observable<ProductCourse>{
    return this.http.get<ProductCourse>(`${environment.localUrl}/projects/${id}`) as Observable<ProductCourse>
  }
  getProductPaginate(page: number): Observable<any>{
    return this.http.post<any>(`${environment.localUrl}get-project-paginate`, page) as Observable<any>
  }
  addToChart(body: any){
    return this.http.post<any>(`${environment.fireBase}cart.json`, body)
  }
  getProducts(){
    return this.http.get<any>(`${environment.fireBase}cart.json`)
  }
  deleteCart(id: any){
    return this.http.delete<any>(`${environment.fireBase}cart.json`, id)
  }
}
