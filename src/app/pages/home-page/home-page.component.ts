import { Component, NgModule, OnInit, Pipe } from '@angular/core'
import { CommonModule } from '@angular/common'
import { NavbarComponent } from '../../components/navbar/navbar.component'
import { ProductsItemsComponent } from '../../components/products-items/products-items.component'
import { SharedDataService } from '../../shared/shared-data.service'
import { ProductsService } from '../../services/products.service'
import { ProductCourse } from 'src/app/shared/products.model'
import { Router } from '@angular/router'
import { FormsModule, NgForm } from '@angular/forms'
import { MatSnackBarModule, MatSnackBar } from '@angular/material/snack-bar'
import { LoadingComponent } from '../../components/loading/loading.component'
import { ImageStarComponent } from '../../components/image-star/image-star.component';

@Component({
  selector: 'app-home-page',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    NavbarComponent,
    ProductsItemsComponent,
    MatSnackBarModule,
    LoadingComponent,
    ImageStarComponent
  ],
  template: `
    <div>
      <div class="banner mt-14">
        <div class="banner-image"></div>
        <div class="title-banner">
          <span class="head-text">LET'S JOIN WITH US</span>
          <P>Improve our skill with us</P>
        </div>
      </div>
      <div class="product-filter-list mt-14">
        <!-- <form #f="ngForm" (ngSubmit)="doFilter(f)"> -->
        <div class="bar-filter">
          <p>Filter By Price</p>
          <select
            [(ngModel)]="selectOption"
            (change)="onOptionChange($event)"
            id=""
          >
            <option ngModel name="hargaTinggi" [value]="99000">
              Price > $100k
            </option>
            <option ngModel name="hargaRendah" [value]="100000">
              Price < $100k
            </option>
          </select>
          <button (click)="clearFilter()" class="btn-filter-reset">
            Clear
          </button>


          <input
            [(ngModel)]="searchText"
            name="name"
            (input)="inputTextSearch($event)"
            type="text"
            name=""
            class="ml-44"
            placeholder="Search Course .."
            id=""
          />
        </div>
        <button (click)="sortdsortArrayObject()" class="btn-filter">
            <span>
              <img
                class="arrow"
                src="../../../assets/asc.png"
                width="30"
                height="30"
                alt=""
              />
            </span>
          </button>
          <button (click)="sortdascArrayObject()" class="btn-filter">
            <span>
              <img
                class="arrow-desc"
                src="../../../assets/asc.png"
                width="30"
                height="30"
                alt=""
              />
            </span>
          </button>
      </div>
      <div class="mt-14 products-list">
        <div *ngIf="isLoading">
          <app-loading></app-loading>
        </div>
        <app-products-items *ngIf="!isLoading">
          <h4 class="text-center text-bold" *ngIf="dataProductList <= 0">
            Data Kosong
          </h4>
          <div *ngFor="let item of dataProductList">
            <div class="grid grid-cols-3 gap-5">
              <div>
                <span
                  class="text-xs label font-semibold inline-block py-1 px-2 rounded text-pink-600 bg-pink-200 last:mr-0 mr-1"
                >
                  {{ showDate(item?.createAt) }}
                </span>

                <img
                  (click)="gotoDetail(item._id)"
                  class="rounded"
                  [src]="item.image"
                  alt=""
                />
                <span
                *ngIf="item.rating >= 4 && item.reviews >= 19"
                  class="text-xs label-hot font-semibold inline-block py-1 px-2 rounded text-pink-600 bg-green-500 last:mr-0 mr-1"
                >
                  {{ labelHotlesson }}
                </span>
              </div>
              <div>
                <b>{{ item.name }}</b>
                <p class="text-description" (click)="gotoDetail(item._id)">
                  {{ item.description | slice: 0:104 }}
                </p>
                <p class="text-teacher">{{ item.teacherName }}</p>
                <!-- <span
                  style="font-weight:bold; color:orange; font-size:14px
                  "
                  *ngIf=""
                >
                  {{ 'Hot Lesson' }}
                </span> -->
                <div class="image-star">
                <b *ngIf="item?.reviews !== 0" class="views">
                    ( {{ item?.reviews }} )
                  </b>
                  <span *ngIf="item?.reviews === 0" class="views">
                    ( No Reviews )
                  </span>
                  {{item.rating}}
                      <app-image-star [checkStar]="start"></app-image-star>
                      <app-image-star [checkStar]="start"></app-image-star>
                      <app-image-star [checkStar]="start"></app-image-star>
                      <app-image-star [checkStar]="start"></app-image-star>
                      <app-image-star [checkStar]="start"></app-image-star>
                </div>
                <p class="hours">
                  {{ item.totalHours }}
                  <span
                    style=" border-left: 2 px solid gray;
        height: 20px;"
                  ></span>
                  <span>{{ item.typeStudent }}</span>
                  <br />
                  <span
                    class="text-xs label font-semibold inline-block py-1 px-2 rounded text-pink-600 bg-amber-300 last:mr-0 mr-1"
                    *ngIf="item.rating >= 4 && item.reviews >= 19"
                  >
                    {{ isBestSeller('BEST SELLER')}}
                  <span *ngIf="item.rating >= 4 && item.reviews >= 19">
                    {{bestSeller}}
                  </span>
                  </span>
                  <span
                    (click)="
                      addToChart(item._id, item.name, item.price, item.image)
                    "
                  >
                    <img
                      style="cursor:pointer; position:absolute; margin-left: 100px"
                      src="../../../assets/icons8-cart-64.png"
                      width="20"
                      height="20"
                      alt=""
                    />
                  </span>
                </p>
              </div>
              <div class="text-end">
                <b>{{ item.price }}</b>
                <br />
                <span style="text-decoration: line-through; color:red">
                  {{ 120 | currency }}
                </span>
              </div>
            </div>
          </div>
        </app-products-items>
        <div class="next mt-14">
          <button class="btn-navigate" *ngIf="page > 1" (click)="pervious()">
            Pervious
          </button>
          <button
            style=" margin-left: 12px;"
            *ngIf="allpage === 4"
            class="btn-navigate"
            (click)="doNavigate()"
          >
            Next
          </button>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./home-page.component.scss'],
})
export class HomePageComponent implements OnInit {
  page: any
  dataProductList: any
  nextpage = 1
  allpage: any
  allData: any
  isLoading = false
  hargaTinggi: any
  hargaRendah: any
  selectOption: any
  searchText: any = ''
  desc = false
  created?: string
  hostLesson = false;
  bestSeller!: string
  labelHotlesson?: string
  start?: string;
  rating?: number
  constructor(
    private shareData: SharedDataService,
    private snackbar: MatSnackBar,
    private productService: ProductsService,
    private router: Router,
  ) {
    this.dogetDataPaginate()
  }

  ngOnInit(): void {}

  dogetDataPaginate() {
    this.isLoading = true
    let pageinate: any = {
      page: this.nextpage,
      hargaTinggi: this.hargaTinggi,
      hargaRendah: this.hargaRendah,
      keyword: this.searchText
    }
    this.productService
      .getProductPaginate(pageinate)
      .subscribe((value: ProductCourse) => {
        this.dataProductList = value.data
        this.dataProductList.map((item: any) => {
          item.rating >= 3 ? this.rating = 3 : this.rating = 4
        } )
        this.isLoading = false
        this.page = value.page
        this.allpage = value.lengthproject
        this.allData = value.pagesAll
      }, error => {
        this.snackbar.open('error get data', '500' , {
          panelClass: ['snackbar-error'],
          duration: 3000,
        })
      })
  }
  doNavigate() {
    this.nextpage++
    this.dogetDataPaginate()
  }
  pervious() {
    this.nextpage--

    this.dogetDataPaginate()
  }

  gotoDetail(id: string) {
    this.router.navigate(['/products-detail', id])
  }
  onOptionChange(event: any) {
    const selectedOption = event.target.value
    const convert = parseInt(selectedOption)
    if (convert === 99000) {
      this.hargaTinggi = convert
      this.dogetDataPaginate()
    } else if (convert === 100000) {
      this.hargaTinggi = null
      this.hargaRendah = convert
      this.dogetDataPaginate()
    }
  }
  clearFilter() {
    this.hargaRendah = null
    this.hargaTinggi = null
    this.searchText = null
    this.page = 1
    this.dogetDataPaginate()
  }

  sortdsortArrayObject() {
    this.dataProductList.sort((a: any, z: any) => a.price - z.price)
  }
  sortdascArrayObject(){
    this.dataProductList.sort((a: any, z: any) => z.price - a.price)
  }
  rescaleStar(number: any) {
    const factor = 0.35
    number *= factor
    if (number > 5) {
      number = 5
    }
    return number
  }

  addToChart(id: string, name: string, price: string, image: string) {
    const bodydata = {
      id: id,
      name: name,
      price: price,
      image: image,
    }
    this.productService.addToChart(bodydata).subscribe(
      () => {
        this.snackbar.open('success add whistlist', '200', {
          panelClass: ['snackbar-succes'],
          duration: 3000,
        })
        setTimeout(() => {
          window.location.reload()
        }, 3000)
      },
      (error) => {
        this.snackbar.open('error add whislist', '200', {
          panelClass: ['snackbar-error'],
          duration: 3000,
        })
      },
    )
  }
  isBestSeller(bestSeller: string){
    console.log(bestSeller)
    this.bestSeller = bestSeller;
    if(this.bestSeller){
      this.start = 'full'
    }else if(!this.bestSeller){
      this.start = 'half'
    }
    if(this.bestSeller === 'BEST SELLER' && this.hostLesson === true){
      this.labelHotlesson = 'HOT LESSON'
    }
  }
  showDate(date: any) {
    const createdAt = new Date(date)
    const oneWeekAgo = new Date(Date.now() - 7 * 24 * 60 * 60 * 1000)

    if (createdAt >= oneWeekAgo) {
      const newProduct = true
      if(newProduct === true){
       this.hostLesson = true
      }
      return 'NEW'
    } else {
      return ''
    }
  }
  doCheckBestSeller() {}
  inputTextSearch(name: any){
    this.searchText = name.target.value
    this.dogetDataPaginate()
  }
}
