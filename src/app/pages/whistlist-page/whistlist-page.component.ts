import { Component, Inject, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MAT_DIALOG_DATA, MatDialogModule } from '@angular/material/dialog';
import {MatTableDataSource, MatTableModule} from '@angular/material/table';
import { ProductsService } from 'src/app/services/products.service';
import { map } from 'rxjs';
import { MatSnackBarModule, MatSnackBar } from '@angular/material/snack-bar'

@Component({
  selector: 'app-whistlist-page',
  standalone: true,
  imports: [CommonModule, MatDialogModule, MatDialogModule, MatTableModule,  MatSnackBarModule],
  template: `
   <div class="example-container mat-elevation-z8 w-full">
   <table class="table-fixed">
  <thead>
    <tr>
      <th class="w-1/2 px-4 py-2">image</th>
      <th class="w-1/4 px-4 py-2">name</th>
      <th class="w-1/4 px-4 py-2">price</th>
      <th class="w-1/4 px-4 py-2">Aksi</th>
    </tr>
  </thead>
  <tbody>
    <tr *ngFor="let item of dataCart">
      <td class="border px-4 py-2">
        <img src="{{item!.image}}" width="80" height="50" alt="">
      </td>
      <td class="border px-4 py-2">{{item!.name}}</td>
      <td class="border px-4 py-2">{{item!.price | currency}}</td>
      <td class="border px-4 py-2">
        <button (click)="removeData(item!.id)">
          <img src="../../../assets/remove.png" width="30" height="40" alt="">
        </button>
      </td>
    </tr>
  </tbody>
</table>
<h4 class="text-center" *ngIf="dataCart?.length <= 0">There is No Data</h4>

</div>
  `,
  styleUrls: ['./whistlist-page.component.scss']
})
export class WhistlistPageComponent implements OnInit {
  displayedColumns = ['image', 'name', 'price', 'delete'];
  dataCart: any;
  constructor(private productService: ProductsService, @Inject(MAT_DIALOG_DATA) public data: any, private snackbar: MatSnackBar) {
    this.dataCart = this.data!.dataDialog
    // this.dataCart.push(this.data?.dataDialog)
    console.log(this.dataCart)
  }



  ngOnInit(): void {

  }

  removeData(id: any) {
    this.productService.deleteCart(id).subscribe((value: any) => {
      this.snackbar.open('success remove', '200', {
        panelClass: ['snackbar-succes'],
        duration: 3000,
      })
      setTimeout(() => {
        window.location.reload()
      }, 3000)
    }, error => {
      this.snackbar.open('error remove', '200', {
        panelClass: ['snackbar-error'],
        duration: 3000,
      })
    })
  }



}
