import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
// import { ProductsDetailRoutingModule } from './products-detail-routing.module';
import { ProductsService } from '../../services/products.service';
import { ProductCourse } from 'src/app/shared/products.model';

@Component({
  selector: 'app-products-detail',
  standalone: true,
  imports: [CommonModule],
  template: `
   <div class="product-detail grid grid-cols-2">
        <div class="image">
            <img [src]="dataDetail?.image" alt="">
        </div>
        <div class="content">
            <strong>{{dataDetail?.name}}</strong>
            <p>{{dataDetail?.description}}</p>
            <span>Instuctor : </span>
            <b style="margin-left: 35px;">{{dataDetail?.teacherName}}</b>
            <br>
            <span>Total Hours : </span>
            <b style="margin-left: 20px;">{{dataDetail?.totalHours}}</b>
            <br>
            <span>For Student : </span>
            <b style="margin-left: 20px;">{{dataDetail?.typeStudent}}</b>
            <br>
            <div class="rating" *ngIf="ratingValue > 4">
              <img src="../../../assets/fullstar.png" width="20" alt="">
              <img src="../../../assets/fullstar.png" width="20" alt="">
              <img src="../../../assets/fullstar.png" width="20" alt="">
              <img src="../../../assets/halfstar.png" width="20" alt="">
              <img src="../../../assets/halfstar.png" width="20" alt="">
              <span *ngIf="dataDetail?.reviews != 0" style="margin-top: 20px;">( {{dataDetail?.reviews}} )</span>
              <span *ngIf="dataDetail?.reviews == 0" style="margin-top: 20px;">( No Reviews )</span>

            </div>
            <br>
            <span>Price :</span>
            <b style="margin-left: 20px;">{{dataDetail?.price | currency}}</b>
            <br>
            <div class="best-seller-box">
              <span *ngIf="messageBest != undefined" class="best-seller">{{messageBest}}</span>
            </div>
          <div class="button-checkout">
            <button class="checkout">Buy</button>
          </div>
        </div>
   </div>
  `,
  styleUrls: ['./products-detail.component.css']
})
export class ProductsDetailComponent implements OnInit {
  id!: string | null;
  dataDetail?: {
    id: string
    name: string
    description: string
    teacherName: string
    rating: number
    totalHours: string
    typeStudent: string
    image: string
    price: number
    reviews?: number | undefined
  }
  rate: any;
  reviewValue: any;
  ratingValue: any;
  messageBest!: string;
  constructor(private actvitedRoure: ActivatedRoute, private productService: ProductsService) { }

  ngOnInit(): void {
    this.id = this.actvitedRoure.snapshot.paramMap.get('id')
    console.log(this.id)
    this.getDataProductById()
  }

  getDataProductById(){
    this.productService.dogetProductById(this.id).subscribe((value: ProductCourse) => {
      console.log(value)
      this.dataDetail = value.data
      if (this.dataDetail && this.dataDetail.reviews && this.dataDetail.rating) {
        this.reviewValue = this.dataDetail.reviews;
        this.ratingValue = this.dataDetail.rating;
        if (this.reviewValue > 10 && this.ratingValue >= 4) {
          this.messageBest = 'BEST SELLER'
        }
      }
    })
  }

}
