import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  standalone: true,
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(value: any, searchText: string, propName: any): any {
    if(value.length === 0){
      return value
    }
    for(const item of value){
      const arr = [];
      if(item[propName] == searchText){
        arr.push(item)
      }
      return arr
    }
  }

}
