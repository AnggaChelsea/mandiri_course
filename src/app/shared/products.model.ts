export interface ProductCourse{
  data : {
    id: string,
    name: string,
    description: string,
    teacherName: string,
    rating: number,
    totalHours: string,
    typeStudent: string,
    image: string,
    price: number
  }
  page: number,
  pages: number,
  pagesAll: number,
  lengthproject: number,
}
