import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedDataService {
private dataSearch = new BehaviorSubject<any>('')
dataGet = this.dataSearch.asObservable()
private paginate = new BehaviorSubject<any>(Number)
paginateData$ = this.paginate.asObservable()

constructor() { }

doShareDataSearchValue(inputText:string){
  return this.dataSearch.next(inputText)
}
getPage(pageNumber: number){
  return this.paginate.next(pageNumber)
}

}
