import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { ProductsDetailComponent } from './pages/products-detail/products-detail.component';
import { WhistlistPageComponent } from './pages/whistlist-page/whistlist-page.component';
const routes: Routes = [
  {
    path: '',
    component: HomePageComponent,
  },
  {
    path: 'products-detail/:id',
    component: ProductsDetailComponent
  },
  {
    path: 'whistlist',
    component: WhistlistPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
