import { Component, Inject, Input, OnInit } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms'
import { SharedDataService } from '../../shared/shared-data.service'
import { RouterModule } from '@angular/router'
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogConfig,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog'
import { WhistlistPageComponent } from '../../pages/whistlist-page/whistlist-page.component'
import { ProductsService } from '../../services/products.service'
import { map } from 'rxjs'

@Component({
  selector: 'app-navbar',
  standalone: true,
  imports: [CommonModule, FormsModule, RouterModule, MatDialogModule],
  template: `
    <div class="navbar ">
      <nav class="bg-gray-800">
        <div class="mx-auto max-w-7xl px-2 sm:px-6 lg:px-8">
          <div class="relative flex h-16 items-center justify-between">
            <div
              class="flex flex-1 items-center justify-center sm:items-stretch sm:justify-start"
            >
              <div class="flex flex-shrink-0 items-center">
                <img
                  class="block h-8 w-auto lg:hidden icon"
                  src="../../../assets/mandiri.png"
                  width="50"
                  height="50"
                  alt="mandiri Banking"
                />
                <img
                  class="hidden h-8 w-auto lg:block"
                  src="../../../assets/mandiribank.png"
                  width="50"
                  height="50"
                  alt="mandiri Banking"
                />
              </div>
              <div class="hidden sm:ml-6 sm:block">
                <div class="flex space-x-4">
                  <a
                    href="#"
                    class="bg-gray-900 text-white px-3 py-2 rounded-md text-sm font-medium"
                    aria-current="page"
                  >
                    Mandiri Banking Learning
                  </a>

                  <a
                    href="#"
                    class="text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-sm font-medium"
                  >
                    Projects
                  </a>

                  <a
                    href="#"
                    class="text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-sm font-medium"
                  >
                    Calendar
                  </a>
                </div>
              </div>
            </div>
            <div
              class="absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0"
            >
              <button
                type="button"
                class="ring rounded-full bg-gray-800 p-1 text-gray-400 hover:text-white focus:outline-none focus:ring-2 focus:ring-white focus:ring-offset-2 focus:ring-offset-gray-800"
                (click)="openDialog()"
              >
                <small class="text-white" style="margin-left:20px;">{{cartlength}}</small>
                <svg
                  class="h-6 w-6"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke-width="1.5"
                  stroke="currentColor"
                  aria-hidden="true"
                >
                  <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    d="M14.857 17.082a23.848 23.848 0 005.454-1.31A8.967 8.967 0 0118 9.75v-.7V9A6 6 0 006 9v.75a8.967 8.967 0 01-2.312 6.022c1.733.64 3.56 1.085 5.455 1.31m5.714 0a24.255 24.255 0 01-5.714 0m5.714 0a3 3 0 11-5.714 0"
                  />
                </svg>
              </button>
            </div>
          </div>
        </div>

        <div class="sm:hidden" id="mobile-menu">
          <div class="space-y-1 px-2 pt-2 pb-3">
            <a
              href="#"
              class="bg-gray-900 text-white block px-3 py-2 rounded-md text-base font-medium"
              aria-current="page"
            >
              Dashboard
            </a>

            <a
              href="#"
              class="text-gray-300 hover:bg-gray-700 hover:text-white block px-3 py-2 rounded-md text-base font-medium"
            >
              Team
            </a>

            <a
              href="#"
              class="text-gray-300 hover:bg-gray-700 hover:text-white block px-3 py-2 rounded-md text-base font-medium"
            >
              Projects
            </a>

            <a
              href="#"
              class="text-gray-300 hover:bg-gray-700 hover:text-white block px-3 py-2 rounded-md text-base font-medium"
            >
              Calendar
            </a>
          </div>
        </div>
      </nav>
    </div>
  `,
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  searchText: any
  animal: any
  cartlength: any
  dataCart: any
  constructor(
    private shareDataService: SharedDataService,
    public dialog: MatDialog,
    private productService: ProductsService,
  ) {}

  ngOnInit(): void {
    this.getData()
  }

  doShareData() {
    console.log(this.searchText)
    const data = {
      name: this.searchText,
    }
    console.log(data)
    this.shareDataService.doShareDataSearchValue(this.searchText)
  }

  getData() {
    this.productService
      .getProducts()
      .pipe(
        map((item: any) => {
          const dataArr = []
          for (const key in item) {
            dataArr.push({ ...item[key], id: key })
          }
          return dataArr
        }),
      )
      .subscribe((value: any) => {
        console.log(value, 'valu')
        this.cartlength = value.length
        this.dataCart = value,
        console.log(this.dataCart)
      })
  }

  openDialog() {
    const dialogConfig = new MatDialogConfig()
    dialogConfig.disableClose = false
    dialogConfig.width = '1000px'
    dialogConfig.data = {
      titleDialog: 'CART COURSE',
      dataDialog: this.dataCart,
    }
    const dialogRef = this.dialog.open(WhistlistPageComponent, dialogConfig)
    dialogRef.afterClosed().subscribe((result: any) => {
      console.log(result)
    })

  }

}
