import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-footer',
  standalone: true,
  imports: [CommonModule],
  template: `
    <footer class="bg-gray-200 text-center lg:text-left inset-x-0 bottom-0">
  <div class="text-gray-700 text-center p-4" style="background-color: rgba(0, 0, 0, 0.2);">
    © 2023 Copyright:
    <a class="text-gray-800" href="https://tailwind-elements.com/">Mandiri bank Learning</a>
  </div>
</footer>
  `,
  styles: [
    `
    footer{
      position:relative;

    }
    `
  ]
})
export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
