import { Component, OnInit, Input } from '@angular/core'
import { CommonModule } from '@angular/common'

@Component({
  selector: 'app-image-star',
  standalone: true,
  imports: [CommonModule],
  template: `
    <img
      *ngIf="checkStar === 'full'"
      src="../../../assets/fullstar.png"
      width="20"
      height="20"
      alt=""
    />

    <img
      *ngIf="checkStar !== 'full'"
      src="../../../assets/icons8-starfish-50.png"
      style="width:20px; height:20px"
      alt=""
    />
  `,
  styles: [],
})
export class ImageStarComponent implements OnInit {
  @Input() checkStar?: string
  constructor() {}

  ngOnInit(): void {}
}
