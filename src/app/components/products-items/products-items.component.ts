import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsService } from '../../services/products.service';
import { ProductCourse } from 'src/app/shared/products.model';
import { Router } from '@angular/router';
import { SharedDataService } from '../../shared/shared-data.service';

@Component({
  selector: 'app-products-items',
  standalone: true,
  imports: [CommonModule],
  template: `
   <div class="mt-14 grid gap-5 products ">
       <ng-content></ng-content>
   </div>
  `,
  styleUrls: ['./products-items.component.scss']
})
export class ProductsItemsComponent implements OnInit {
  dataProductList: any;
  page!: number;
  constructor(private productService: ProductsService, private router: Router, private dataShareService: SharedDataService) { }
  ngOnInit(): void {
    // this.dogetDataPaginate()
    this.dataShareService.dataGet.subscribe((value: any) => {
      console.log(value);
    })
    this.dataShareService.paginateData$.subscribe((value: any) => {
      console.log(value, 'producrs');
      this.page = value
    })
}


  getDataProductList(){
    this.productService.getProduct().subscribe((value: ProductCourse) => {
      this.dataProductList = value.data
      console.log(this.dataProductList);
    })
  }





}
